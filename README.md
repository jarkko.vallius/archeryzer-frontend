# About Archeryzer

Archeryzer is a React application, using bluetooth sensor to gather and analyze bow motion data during a shot process.

## Specifications
- React frontend
- Ruuvitag sensor with modified firmware
- Google Firebase for data storage


# Dev things

## Firestore data

### User
* name

### Session
- id
- created
- updated
- shotCount
- config
- SETS

### Set
- id
- created
- updated
- shots
- shotCount


## Firestorage data

/session_id/set_id.json

### Data
- timestamp
- x
- y
- z
- ...


