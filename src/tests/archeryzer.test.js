const Archeryzer = require('../components/archeryzer/archeryzer')
const _ = require('lodash')

function createPromise() {
    var _resolve, _reject;
    var promise = new Promise(function (resolve, reject) {
        _resolve = resolve;
        _reject = reject;
    })
    promise.resolve = _resolve;
    promise.reject = _reject;
    return promise;
}

const dataObj = (timestamp, x, y, z) => {
    return {
        timestamp,
        x,
        y,
        z
    }
}

const removeKeys = arr => {
    return arr.map(e => _.pick(e, ['timestamp', 'x', "y", "z"]))
}


const runTwice = async (input, config) => {
    const results = createPromise()
    const archeryzer = Archeryzer({
        onEvents: events1 => {
            const setEnd1 = events1.find(e => e.name === "set_end")
            if (setEnd1) {
                const archeryzer2 = Archeryzer({
                    onEvents: events2 => {
                        const setEnd2 = events2.find(e => e.name === "set_end")
                        if (setEnd2) {
                            results.resolve([setEnd1.data, setEnd2.data])
                        }
                    }
                })
                archeryzer2.init(config)
                _.map(setEnd1.data, e => archeryzer2.push(e))
            }

        }
    })
    archeryzer.init(config)
    _.map(input, e => archeryzer.push(e))
    return await results
}


test('multiple runs gives same results', async () => {
    const config = {
        peakTimeFrame: [0, 1],
        peakMotionThreshold: 4,
        peakThreshold: 100,
        positionTimeFrame: [0, 10],
        positionLimit: {
            x: [0, 10],
            y: [0, 10],
            z: [0, 10]
        },
        idleToActiveTime: 3,
        activeToIdleTime: 3,
        cooldownTime: 1,
    }

    let input = [
        dataObj(1, 0, 0, 0),    // -1
        dataObj(2, 0, 0, 0),    // -1
        dataObj(3, 0, 0, 0),    // -1
        dataObj(4, 0, 0, 0),    // 0
        dataObj(5, 0, 0, 0),    // 0        
        dataObj(6, 0, 0, 0),    // 0        
        dataObj(7, 20, 20, 20), // 0 -> 1  -1 
        dataObj(8, 10, 10, 10), // 0 -> 1  -1
        dataObj(9, 20, 20, 20), // 0 -> 1  -1
        dataObj(10, 10, 10, 10),// 1        1
        dataObj(11, 20, 20, 20),// 1        1 
        dataObj(12, 0, 0, 0),   // 1        1
        dataObj(13, 0, 0, 0),   // 1 -> 0   1
        dataObj(14, 0, 0, 0),   // 1 -> 0   1
        dataObj(15, 0, 0, 0),   // 1 -> 0   1
        dataObj(16, 0, 0, 0),   // 0 end    0
    ]

    let expectedOutput = [
        dataObj(7, 20, 20, 20), // 0 -> 1  -1 
        dataObj(8, 10, 10, 10), // 0 -> 1  -1
        dataObj(9, 20, 20, 20), // 0 -> 1  -1
        dataObj(10, 10, 10, 10),// 1        1
        dataObj(11, 20, 20, 20),// 1        1 
        dataObj(12, 0, 0, 0),   // 1        1
        dataObj(13, 0, 0, 0),   // 1 -> 0   1
        dataObj(14, 0, 0, 0),   // 1 -> 0   1
        dataObj(15, 0, 0, 0),   // 1 -> 0   1
        dataObj(16, 0, 0, 0),   // 0 end    0
    ]


    await runTwice(input, config)
        .then(results => {
            expect(results[0]).toEqual(results[1])
            const firstResults = results[0]
            expect(removeKeys(firstResults)).toEqual(expectedOutput)

            // in the end bowState must be 0
            expect(_.last(firstResults).bowState).toEqual(0)
        })
});