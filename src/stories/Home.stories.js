import React from 'react'
import { HomeView } from '../components/home/HomeView'


export default {
    title: "Home"
}

export const Home = () => <HomeView />