import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';
import { useStorage } from '../hooks/useStorage'
import { Button } from '@material-ui/core'
// import testdata from '../testdata/set.json'
export default {
    title: 'Storage',
};

export const Upload = () => {
    const { download, upload } = useStorage()
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const path = "test/helloworld.json"
    const content = {
        name: "Hello",
        data: "World"
    }

    const onUpload = () => {
        setLoading(true)
        upload({ path: path, data: content })
            .then(results => console.log("results", results))
            .catch(error => console.error(error))
            .finally(() => setLoading(false))
    }

    const onDownload = () => {
        setLoading(true)
        download({ path: path })
            .then(results => {
                console.log("results", results)
                setData(results)
            })
            .catch(error => console.error(error))
            .finally(() => setLoading(false))
    }
    return (
        <>
            <pre>path {JSON.stringify(path)}</pre>
            <pre>content {JSON.stringify(content)}</pre>
            <pre>loading = {JSON.stringify(loading)}</pre>

            <Button onClick={() => onUpload()}>upload</Button>
            <Button onClick={() => onDownload()}>download</Button>

            {data && <div>DATA = {JSON.stringify(data)}</div>}
        </>
    )
}
