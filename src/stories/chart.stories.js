import React from 'react';
import testdata from './mockdata/chartdata.json'
import { LineGraph } from '../components/LineGraph'
export default {
    title: 'Chart',
};

const shots = [
    {
        "x": -1992,
        "y": 1300,
        "z": -1992,
        "timestamp": 1588138074592,
        "peak": 408757,
        "posX": 1101,
        "posY": -97,
        "posZ": 129,
        "bowState": 83,
        "timeLeft": null,
        "fromState": null,
        "toState": null,
        "moving": true
    },
]


export const TestData = () => {


    return (
        <>
            <LineGraph
                data={testdata}
                dataKey={"peak"}
                shots={shots}
            />
        </>
    )
}

export const ChartExampl = () => {

    const data = [
        { timestamp: 1, peak: 1 },
        { timestamp: 2, peak: 3 },
        { timestamp: 3, peak: 2 },
        { timestamp: 4, peak: 1 },
        { timestamp: 5, peak: 10 },
        { timestamp: 6, peak: 2 },
        { timestamp: 7, peak: 1 },
        { timestamp: 8, peak: 4 },
        { timestamp: 9, peak: 2 }
    ]

    return (
        <>
            <LineGraph
                data={data}
                dataKey={"peak"}
                shots={[
                    { timestamp: 5 },
                    { timestamp: 7 }
                ]}
            />
        </>
    )
}
