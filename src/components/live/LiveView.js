import React from 'react'
import { Container, Typography } from '@material-ui/core'
import _ from 'lodash'
import { useArcheryzerData } from '../../hooks/useArcheryzerData'
import { useComponentState } from '../../hooks/useComponentState'
import { LiveSession } from './LiveSession'

const defaultConfig = require('../archeryzer/defaultConfig')

const LiveView = props => {
    const { onBackClick } = props
    const { setSession, uploadSetData, setSet } = useArcheryzerData()
    const { setLoading, setError } = useComponentState()

    const onSessionStart = payload => {
        const { session } = payload
        console.log("onSessionStart", session)
        setLoading(true)
        setSession({ session: session })
            .catch(err => setError(err))
            .finally(() => setLoading(false))
    }

    const onSessionEnd = payload => {
        const { session } = payload
        console.log("onSessionEnd", session)
        setLoading(true)
        setSession({ session: session })
            .catch(err => setError(err))
            .finally(() => setLoading(false))
    }

    const onSetEnd = payload => {
        const { session, set } = payload
        console.log("onSetEnd", payload)
        setLoading(true)
        uploadSetData({ setId: set.id, sessionId: session.id, data: set.data })
            .then(() => _.omit(set, ['data']))
            .then((setWithoutData) => setSet({ set: setWithoutData, session: session }))
            .then(() => setSession({ session: session }))
            .catch(err => setError(err))
            .finally(() => setLoading(false))
    }
    return (
        <>
            <Container>
                <Typography variant="h1">Live</Typography>
                <LiveSession
                    config={defaultConfig}
                    onSessionStart={onSessionStart}
                    onSessionEnd={onSessionEnd}
                    onSetEnd={onSetEnd}
                    onBackClick={onBackClick}
                />
            </Container>
        </>
    )
}

export { LiveView }
