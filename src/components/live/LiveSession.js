import { Box, Button, makeStyles, Typography } from '@material-ui/core'
import clsx from 'clsx'
import _ from 'lodash'
import { toJS } from 'mobx'
import { Observer, useLocalStore } from 'mobx-react-lite' // 6.x or mobx-react-lite@1.4.0
import moment from 'moment'
import React, { useEffect } from 'react'
import { v4 as uuid } from 'uuid'
import useBluetooth from '../../hooks/useBluetooth'
import { LineGraph } from '../LineGraph'
const parser = require('../archeryzer/parser')
const Archeryzer = require('../archeryzer/archeryzer')
const BowState = require('../archeryzer/BowState')


const useStyles = makeStyles(() => ({
    running: {
        backgroundColor: "green",
        color: "white"
    }
}))


const getBowState = id => {
    return _.findKey(BowState, value => id === value)
}

const DeviceStatus = ({ name, device, error }) => {
    const classes = useStyles()
    return (

        <Observer>
            {() =>
                <>
                    <Box className={clsx({ [classes.running]: name === "connected" })}>
                        <Typography>bluetoothDevice: {device && device.name}</Typography>
                        <Typography>bluetoothStatus: {name}</Typography>
                        {error && <Typography color="secondary">error: {error.toString()}</Typography>}
                    </Box>
                </>
            }
        </Observer>

    )
}

const SessionStatus = ({ id, sets, shotCount, started, ended }) => {
    return (
        <>
            <Typography variant="h2">Session</Typography>
            <Typography>id: {id}</Typography>
            <Typography>sets: {sets.length}</Typography>
            <Typography>shotCount: {shotCount}</Typography>
            <Typography>started: {started}</Typography>
            <Typography>ended: {ended}</Typography>
        </>
    )
}

const SetStatus = ({ id, shotCount, started, ended, duration }) => {
    // const classes = useStyles()
    return (
        <>
            <Box >
                <Typography variant="h2">Set</Typography>
                <Typography>id: {id}</Typography>
                <Typography>shotCount: {shotCount}</Typography>
                <Typography>started: {started}</Typography>
                <Typography>ended: {ended}</Typography>
            </Box>

        </>
    )
}

const BowStatus = ({ state, running, changing, time }) => {
    const classes = useStyles()
    return (
        <>
            <Box className={clsx({ [classes.running]: running })}>
                <Typography variant="h2">BowStatus</Typography>
                <Typography>bowState: {state}</Typography>
                <Typography>running: {JSON.stringify(running)}</Typography>
                <Typography>changing: {changing}</Typography>
                <Typography>time: {time}</Typography>
            </Box>

        </>
    )
}

let archeryzer = null

const LiveSession = props => {
    const { config, onSessionStart, onSessionEnd, onBackClick, onSetEnd } = props


    let sensorStore = useLocalStore(() => ({
        status: null,
    }))

    let rawDataStore = useLocalStore(() => ({
        data: [],
        timestamp: moment(),
        pushData: d => rawDataStore.data.push(d),
        refreshDataRate: () => {
            const timestamp = moment(rawDataStore.timestamp)
            const data = JSON.parse(JSON.stringify(rawDataStore.data))
            const diff = moment.duration(moment().diff(moment(timestamp))).asSeconds()
            //rawDataStore.graphData.replace(data)

            rawDataStore.data = []
            rawDataStore.timestamp = moment()

            rawDataStore.dataRate = _.floor(data.length / diff)

        },
        dataRate: "0",
        intervalId: null,
        // graphData: [],


    }))

    let sessionStore = useLocalStore(() => ({
        id: null,
        started: null,
        ended: null,
        state: null,
        shotCount: 0,
        sets: [],
        //config: config,
        addSet: newSet => {
            let cpy = JSON.parse(JSON.stringify(newSet))
            delete cpy.data
            sessionStore.sets.push(cpy)
            sessionStore.shotCount += cpy.shotCount
        },
        start: () => {
            sessionStore.id = uuid()
            sessionStore.started = moment().toISOString()
            sessionStore.state = "stand_by"
        },
        endSession: () => {
            sessionStore.state = "ended"
            sessionStore.ended = moment().toISOString()
        },
    }))

    let bowStateStore = useLocalStore(() => ({
        bowState: null,
        transition: null,
        setRunning: null,
        stateChanging: null,
        timeLeft: null,
        clearAll: () => {
            bowStateStore.bowState = null
            bowStateStore.transition = null
            bowStateStore.setRunning = null
            bowStateStore.stateChanging = null
            bowStateStore.timeLeft = null
        }
    }))

    let setStore = useLocalStore(() => ({
        id: null,
        started: null,
        ended: null,
        shotCount: 0,
        shots: [],
        data: null,
        startSet: () => {
            setStore.id = uuid()
            setStore.started = moment().toISOString()
            setStore.ended = null
            setStore.data = null
            setStore.shots = []
            setStore.shotCount = 0
            bowStateStore.setRunning = true
            sessionStore.state = "shooting"
        },
        endSet: (data) => {
            setStore.data = data
            bowStateStore.setRunning = false
            sessionStore.state = "stand_by"
            setStore.ended = moment().toISOString()
            setStore.duration = moment.duration(moment(setStore.ended).diff(moment(setStore.started)))
        },
        pushShot: payload => {
            setStore.shots.push(payload)
            setStore.shotCount++
        },
        pushData: payload => setStore.data.push(payload),
        clearData: () => setStore.data = null
    }))

    const onStartSession = () => {
        sessionStore.start()
        const cpy = JSON.parse(JSON.stringify(toJS(sessionStore)))
        onSessionStart({ session: cpy })
    }

    const onEndSession = () => {
        sessionStore.endSession()
        const cpy = JSON.parse(JSON.stringify(toJS(sessionStore)))
        onSessionEnd({ session: cpy })
    }

    const onEndSet = (event) => {
        setStore.endSet(event.data)
        const setCpy = JSON.parse(JSON.stringify(toJS(setStore)))
        sessionStore.addSet(setCpy)
        const sessionCpy = JSON.parse(JSON.stringify(toJS(sessionStore)))
        onSetEnd({ session: sessionCpy, set: setCpy })
    }


    useEffect(() => {
        // reaction(() => JSON.stringify(sessionStore), json => {
        //     sessionStorage.setItem('sessionStore', json);
        // }, {
        //     delay: 500,
        // });
        // let json3 = sessionStorage.getItem('sessionStore');
        // if (json3) {
        //     Object.assign(sessionStore, JSON.parse(json3));
        // }
    }, [])


    const onEvents = events => {
        if (events.length > 0) {
            events.forEach(e => {
                //console.log(e)
                switch (e.name) {
                    // bow state changed
                    case "init":
                    case "state_change":
                        bowStateStore.bowState = getBowState(e.data.current.bowState)
                        break;
                    case "state_transition":
                        const from = getBowState(e.data.current.fromState)
                        const to = getBowState(e.data.current.toState)
                        const time = e.data.current.timeLeft
                        bowStateStore.stateChanging = from + " => " + to
                        bowStateStore.timeLeft = time
                        break;
                    case "set_start":
                        setStore.startSet(e)
                        break;
                    case "set_end":
                        onEndSet(e)
                        break;
                    case "shot":
                        setStore.pushShot(e.data.current)
                        break;
                    default:
                    // nothing
                }
            })
        }
    }

    useEffect(() => {
        console.log("sessionStarted", sessionStore.started)
    }, [])


    /**
     * onData
     * 
     * handle raw data from bluetooth device
     * 
     */
    const onData = payload => {
        if (archeryzer) {
            const dataObject = parser.parseRawData(payload)
            if (!(dataObject instanceof Error)) {
                //console.log(dataObject)

                const aData = archeryzer.push(dataObject)

                if (aData) {
                    rawDataStore.pushData(aData)
                    //rawDataStore.pushGraphData(aData)

                }
                //setStore.pushData(aData)
            }
        } else {
            console.error("archeryzer undefined")
        }
    }

    /**
     * onStatusChange
     * 
     * handle bluetooth device status changes
     * 
     * @param {*} status 
     */
    const onStatusChange = (status) => {
        console.log("onStatusChange", status)
        sensorStore.status = status
        switch (status.name) {
            case "connected":
                bowStateStore.clearAll()
                console.log("CONNECTED, init archyryzer")
                const a = Archeryzer({ onEvents: onEvents })
                a.init(config)
                archeryzer = a
                const intervalId = setInterval(() => {
                    rawDataStore.refreshDataRate()
                }, 1000)
                rawDataStore.intervalId = intervalId
                break;
            case "disconnected":
                bowStateStore.clearAll()
                // handle disconnection
                clearInterval(rawDataStore.intervalId)
                rawDataStore.intervalId = null

                break;
            default:
        }
    }

    const { connect, disconnect } = useBluetooth({ onStatusChange, onData })

    return (
        <>
            <Button
                variant="contained"
                color="primary"
                disabled={sessionStore.started != null}
                onClick={() => onStartSession()}>
                start session
            </Button>
            <Button
                variant="contained"
                color="primary"
                disabled={sessionStore.started === sessionStore.ended}
                onClick={() => onEndSession()}>
                end session
            </Button>
            <Button
                variant="text"
                color="primary"
                onClick={() => onBackClick()}>
                go back
            </Button>
            <Observer>
                {() =>
                    <>
                        {
                            sensorStore.status != null &&
                            <>
                                <DeviceStatus {...sensorStore.status} />

                                {
                                    sensorStore.status.name !== "connected" &&
                                    <Button
                                        variant="contained"
                                        disabled={sensorStore.status.name === "connecting"}
                                        color="primary"
                                        onClick={() => connect()}>
                                        connect
                                    </Button>
                                }
                                {
                                    sensorStore.status.name === "connected" &&
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={() => disconnect()}>
                                        disconnect
                                    </Button>
                                }
                            </>
                        }
                    </>
                }
            </Observer>

            <Box py={2}>
                <Observer>
                    {() =>
                        <>
                            <Typography>Bluetooth avg data per second</Typography>
                            <Typography style={{ fontWeight: "bold" }}>{rawDataStore.dataRate} </Typography>
                        </>
                    }
                </Observer>
            </Box>
            {/* 
            <Box py={2}>
                <Observer>
                    {() =>
                        <>
                            {false && JSON.stringify(rawDataStore.graphData)}
                            <LineGraph
                                data={rawDataStore.graphData}
                                dataKey={"peak"}
                            />
                        </>
                    }
                </Observer>
            </Box> */}



            <Observer>
                {() => <SessionStatus
                    {...sessionStore}
                />}
            </Observer>
            <Observer>
                {() => <SetStatus
                    {...setStore}
                />}
            </Observer>
            <Observer>
                {() => <BowStatus
                    state={bowStateStore.bowState}
                    running={bowStateStore.setRunning}
                    changing={bowStateStore.stateChanging}
                    time={bowStateStore.timeLeft}
                />}
            </Observer>


            <pre>{JSON.stringify(config, 0, 2)}</pre>
        </>
    )
}

export { LiveSession }
