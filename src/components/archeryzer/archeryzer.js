const { add, calculate, timeBuffer } = require('./helpers')
const _ = require('lodash')
const BowState = require('./BowState')

const clearCalculatedValues = b => {
    let first = _.first(b)
    let keys = _.keys(first)
    let keysToNull = _.difference(keys, ["timestamp", "x", "y", "z", "bowState"])

    return _.map(b, e => {
        return _.assign(
            {},
            e,
            _.zipObject(keysToNull, _.times(keysToNull.length, _.constant(null))),
            {
                "bowState": BowState.initializing
            }
        )
    })
}

/**
 * Calculate and add Position to DataObject
 * 
 * @param {*} dataObject 
 * @param {*} buffer 
 * @param {*} timeFrame 
 */
const addPosition = (dataObject, buffer, config) => {
    const calc = (key) => calculate({
        array: buffer,
        type: "avg",
        key: key,
        timeFrame: config.positionTimeFrame
    })
    return add(dataObject,
        ["posX", "posY", "posZ"],
        [calc("x"), calc("y"), calc("z")]
    )
}

/**
 * Calculate and add Peak to DataObject
 * 
 * @param {*} dataObject 
 * @param {*} buffer 
 * @param {*} timeFrame 
 */
const addPeak = (dataObject, buffer, config) => {
    const calc = (key, bias) => calculate({
        array: buffer,
        type: "variance",
        bias: false,
        key: key,
        timeFrame: config.peakTimeFrame
    })

    const value = _.round(_.mean([calc("x"), calc("y"), calc("z")]))
    return add(dataObject, ["peak"], [value])
}

const isInitialized = (buffer, config) => {
    if (buffer.size() > 1) {
        let first = _.first(buffer.get())
        let last = _.last(buffer.get())
        let t = config.idleToActiveTime - (last.timestamp - first.timestamp)
        //console.log("INITIALIZING ", t)
        return t <= 0
    } else return false
}

const isMoving = (dataObject, config) => {
    return dataObject.peak > config.peakMotionThreshold
}


const isPeakGreaterThanThreshold = (dataObject, config) => {
    return dataObject.peak > config.peakThreshold
}

const isBowPositionWithinLimits = (dataObject, config) => {
    let limits = [
        dataObject.posX > config.positionLimit.x[0],
        dataObject.posX < config.positionLimit.x[1],
        dataObject.posY > config.positionLimit.y[0],
        dataObject.posY < config.positionLimit.y[1],
        dataObject.posZ > config.positionLimit.z[0],
        dataObject.posZ < config.positionLimit.z[1],
    ]
    const isInLimits = _.every(limits, l => l)
    //console.log("isBowPositionWithinLimits", isInLimits)
    return isInLimits
}

const isShot = (dataObject, config) => {
    if (isPeakGreaterThanThreshold(dataObject, config)) {
        if (isBowPositionWithinLimits(dataObject, config)) {
            return true
        }
    }
    return false
}

const addMoving = (dataObject, config) => {

    return add(dataObject,
        ["moving"],
        [isMoving(dataObject, config)]
    )
}



/**
 * Add BowState to DataObject
 * 
 * @param {*} dataObject 
 * @param {*} stateBuffer 
 * @param {*} previousTimestamp 
 * @param {*} config 
 * @param {*} onStateChange 
 */
const addBowState = (dataObject, buffer, stateBuffer, previousTimestamp, config) => {
    const prevObj = _.last(stateBuffer.get())
    let bowState = null
    let timeLeft = null
    let fromState = null
    let toState = null
    let timestamp = _.assign({}, previousTimestamp)



    if (prevObj != null) {
        bowState = prevObj.bowState
        switch (prevObj.bowState) {
            case BowState.initializing:
                // if (buffer.isFull()) {
                if (buffer.size() > 1) {
                    let first = _.first(buffer.get())
                    let last = _.last(buffer.get())
                    let t = config.idleToActiveTime - (last.timestamp - first.timestamp)

                    if (t <= 0) {
                        // init done, change state
                        if (isMoving(dataObject, config)) {
                            bowState = BowState.active
                        } else {
                            bowState = BowState.idle
                        }
                    } else {
                        fromState = BowState.initializing
                        timeLeft = t
                    }
                    //console.log("INITIALIZING ", t)
                    //return t <= 0
                }

                if (isInitialized(buffer, config)) {

                } else {
                    fromState = BowState.initializing

                }
                break;
            case BowState.idle:

                if (isMoving(dataObject, config)) {
                    // IDLE -> ACTIVE
                    if (timestamp.idle == null) {
                        timestamp.idle = dataObject.timestamp
                    }

                    let t = config.idleToActiveTime - (dataObject.timestamp - timestamp.idle)
                    //console.log("IDLE -> ACTIVE ", t)
                    if (t <= 0) {
                        bowState = BowState.active
                    } else {
                        timeLeft = t
                        fromState = BowState.idle
                        toState = BowState.active
                    }
                } else {
                    // IDLE -> IDLE
                    timestamp.idle = null
                }
                break;
            case BowState.active:
                // ACTIVE -> SHOT
                if (isShot(dataObject, config)) {
                    bowState = BowState.shot
                } else if (!isMoving(dataObject, config)) {
                    // ACTIVE -> IDLE
                    if (timestamp.active == null) {
                        timestamp.active = dataObject.timestamp
                    }
                    let t = config.activeToIdleTime - (dataObject.timestamp - timestamp.active)
                    if (t <= 0) {
                        bowState = BowState.idle
                    } else {
                        timeLeft = t
                        fromState = BowState.active
                        toState = BowState.idle
                    }
                } else {
                    // ACTIVE -> ACTIVE
                    timestamp.active = null
                }
                break;
            case BowState.cooldown:

                let t = config.cooldownTime - (dataObject.timestamp - timestamp.cooldown)
                if (t <= 0) {
                    bowState = BowState.active
                } else {
                    timeLeft = t
                    fromState = BowState.cooldown
                    toState = BowState.active
                }

                break;
            case BowState.shot:
                timestamp.cooldown = dataObject.timestamp
                timeLeft = config.cooldownTime
                fromState = BowState.cooldown
                toState = BowState.active
                bowState = BowState.cooldown
                break;
            default:
                throw new Error("Invalid bow state", prevObj.bowState)
        }
    } else {
        //console.log("previous dataObject null")
        bowState = BowState.initializing
    }

    return [
        add(dataObject,
            [
                "bowState",
                "timeLeft",
                "fromState",
                "toState"
            ],
            [
                bowState,
                timeLeft,
                fromState,
                toState
            ]),
        timestamp
    ]
}

const constructEvents = (current, previous) => {
    let events = []
    if (previous == null) {
        events.push({ name: "init", data: { current } })
    } else {
        if (previous.bowState !== current.bowState) {
            events.push({ name: "state_change", data: { current, previous } })
        }
        if (current.bowState === BowState.shot) {
            events.push({ name: "shot", data: { current } })
        }
        if (current.bowState === BowState.cooldown) {
            events.push({ name: "cooldown", data: { current } })
        }
        if (current.timeLeft) {
            events.push({ name: "state_transition", data: { current } })
        }
    }

    return events

}

/**
 * Archeryzer
 * 
 */
const Archeryzer = ({ onEvents = () => { } }) => {

    let timestamp = {
        idle: null,
        active: null,
        cooldown: null
    }

    let config
    let buffer
    let stateBuffer
    let dataBuffer = []
    let writeEnabled = false

    // const updateTimestamps = payload => {
    //     timestamp = _.assign({}, payload[1])
    //     return _.assign({}, payload[0])
    // }

    const getKeys = () =>
        ["peak", "posX", "posY", "posZ", "bowState", "timeLeft", "fromState", "toState"]

    let obj = {}
    obj.init = (configuration) => {
        config = configuration
        buffer = timeBuffer(config.idleToActiveTime)
        stateBuffer = timeBuffer(config.idleToActiveTime)
    }
    obj.getBuffer = () => stateBuffer.get()
    obj.push = dataObject => {
        if (!config) throw new Error("config is undefined, run archeryzer.init(config) first")
        return _.flow([
            d => add(d, getKeys(), _.times(getKeys().length, _.constant(null))),
            d => buffer.push(d),
            () => _.last(buffer.get()),
            d => addPosition(d, buffer.get(), config),
            d => addPeak(d, buffer.get(), config),
            d => addMoving(d, config),
            d => addBowState(d, buffer, stateBuffer, timestamp, config),
            // arr => updateTimestamps(arr),
            //notifyEvents,
            arr => {
                timestamp = _.assign({}, arr[1])
                const d = _.assign({}, arr[0])
                stateBuffer.push(d)
                return d
            },
            d => {
                let current = d
                let previous = null
                if (stateBuffer.size() > 1) {
                    const res = _.takeRight(stateBuffer.get(), 2)
                    previous = res[0]
                }

                if (writeEnabled) {
                    dataBuffer.push(current)
                }

                let events = constructEvents(current, previous)

                // set start
                if (current.bowState == BowState.active) {
                    if (previous.bowState <= BowState.idle) {
                        events.push({ name: "set_start" })
                        dataBuffer = clearCalculatedValues(stateBuffer.get())
                        writeEnabled = true
                    }
                }
                // set end
                if (writeEnabled &&
                    previous.bowState == BowState.active &&
                    current.bowState == BowState.idle) {
                    events.push({ name: "set_end", data: dataBuffer })
                    writeEnabled = false
                    dataBuffer = []
                }

                onEvents(events)

                return d
            }
        ])(dataObject)
    }
    return obj
}

module.exports = Archeryzer