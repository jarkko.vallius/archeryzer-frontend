const config = {
    peakMotionThreshold: 40,
    peakThreshold: 300000,
    peakTimeFrame: [0, 100],

    positionTimeFrame: [300, 500],
    positionLimit: {
        x: [500, 1500],
        y: [-800, 200],
        z: [-800, 300]
    },
    idleToActiveTime: 2000,
    activeToIdleTime: 3000,
    cooldownTime: 3000,
}

module.exports = config