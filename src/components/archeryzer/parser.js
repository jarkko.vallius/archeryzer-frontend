const _ = require('lodash')
const moment = require('moment')

/**
 * Remove unnecessary characters from payload
 * 
 * @param {*} payload 
 */
const clean = payload => {
    let str = ""
    payload.forEach(d => str += String.fromCharCode(d))
    str = str.trim()
    str = str.replace(" ", "")
    str = str.replace(">", "")
    str = str.replace("\n", "")
    str = str.replace("\r", "")
    return str
}

const clean2 = payload => {

    let str = ""
    for (let i = 0; i < payload.byteLength; i++) {
        str += String.fromCharCode(payload.getInt8(i));
    }
    //payload.forEach(d => str += String.fromCharCode(d))
    str = str.trim()
    str = str.replace(" ", "")
    str = str.replace(">", "")
    str = str.replace("\n", "")
    str = str.replace("\r", "")
    // console.log(str)
    return str
}

/**
 * Decode accelerometer data string
 * 
 * Returns an array containing accelerometer integer values:
 * array[0] : x-axis 
 * array[1] : y-axis
 * array[2] : z-axis
 * 
 * Returns null on decoding error
 * 
 * @param {string} payload accelerometer data string
 * @returns {array} accelerometer value array
 */
const decode = payload => {
    let first = null
    let last = null
    let decodedObj = null

    for (let i = 0; i < payload.length; i++) {
        let c = payload.charAt(i)
        if (c == "?") first = i
        if (c == ";") last = i
    }

    function convertHexToAccelerometerValue(e) {
        // on sensor encoding we increase values by 2048,
        // so here we must do the opposite
        return parseInt(e, 16) - 2048
    }

    if (first != null && last != null) {
        let sliced = payload.substring(first + 1, last)
        let splitted = sliced.split(":")
        decodedObj = splitted.map(e => convertHexToAccelerometerValue(e))
    } else {
        // TODO: add stream buffer
    }

    return decodedObj
}

const convertToObject = payload => {
    return {
        x: payload[0],
        y: payload[1],
        z: payload[2]
    }
}


const addTimestamp = payload => {
    return _.assign(payload, { timestamp: moment.now() })
}

const parseRawData = raw => {
    const flow = (d) => _.flow([
        clean2,
        decode,
        convertToObject,
        addTimestamp
    ])(d)
    return _.attempt(flow, raw)
}

module.exports = {
    clean,
    decode,
    convertToObject,
    addTimestamp,
    parseRawData
}
