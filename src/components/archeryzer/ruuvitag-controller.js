const noble = require('noble-mac')

let sensor = null
let rxCharacteristic
let txCharacteristic
let onDeviceConnect = () => { }
let onReceiveData = () => { }
let deviceAddress
let initialized = false

function createPromise() {
    var _resolve, _reject;
    var promise = new Promise(function (resolve, reject) {
        _resolve = resolve;
        _reject = reject;
    })
    promise.resolve = _resolve;
    promise.reject = _reject;
    return promise;
}

let noblePowered = createPromise()

function discoverServices() {
    console.log("Discovering services...")
    sensor.discoverAllServicesAndCharacteristics(
        function (error, services, characteristics) {
            let btUARTService = services.find(e => e.uuid == "6e400001b5a3f393e0a9e50e24dcca9e")
            txCharacteristic = characteristics.find(e => e.uuid == "6e400002b5a3f393e0a9e50e24dcca9e")
            rxCharacteristic = characteristics.find(e => e.uuid == "6e400003b5a3f393e0a9e50e24dcca9e")

            if (error || !btUARTService || !txCharacteristic || !rxCharacteristic) {
                console.log("BT> ERROR getting services/characteristics");
                console.log("Error ", error);
                console.log("Service ", btUARTService);
                console.log("TX ", txCharacteristic);
                console.log("RX ", rxCharacteristic);
                sensor.disconnect()
            }

            rxCharacteristic.on('data', onReceiveData);
            rxCharacteristic.subscribe(function () {
                console.log("Connected to ", sensor.address)
                onDeviceConnect(sensor)
            });
        }
    )
}


const disconnect = () => {
    if (sensor) {
        rxCharacteristic.unsubscribe()
        sensor.disconnect()
        sensor = undefined
        rxCharacteristic = undefined
        txCharacteristic = undefined
    }
}

const init = ({ onData = () => { }, onConnect = () => { } }) => {
    onReceiveData = onData
    onDeviceConnect = onConnect

    if (!initialized) {
        noble.on('stateChange', function (state) {
            console.log("Noble: stateChange -> ", state);
            if (state == "poweredOn") {
                console.log("Noble: Powered")
                noblePowered.resolve(true)
            }
        });

        noble.on('discover', function (device) {
            if (device.address) {
                console.log("Noble: found device : ", device.address)
                if (device.address == deviceAddress) {
                    // connect
                    sensor = device
                    noble.stopScanning()
                    sensor.connect(function (error) {
                        console.log("Connecting...")
                        if (error) {
                            console.error("Connection error", error);
                            sensor = undefined;
                        } else {
                            discoverServices()
                        }
                    })
                    sensor.once('disconnect', () => {
                        console.log("Noble: sensor disconnected")
                    });
                } else {
                    console.log("ERROR: Device address mismatch : ", device.address, " - ", deviceAddress)
                }
            }
        });
        noble.on('scanStart', function () {
            console.log("Noble: scanStart")
        });
        noble.on('scanStop', () => {
            console.log("Noble: scanStop")
        });
        initialized = true
    } else {
        console.log("ruuvitag-controller already initialized!")
    }




}


const connect = (address) => {
    deviceAddress = address
    noblePowered.then(() => {
        noble.startScanning([], false);
    })

}
// function connect({
//     deviceAddress,
//     onConnect = () => { },
//     onData = () => { }
// }) {



// }

module.exports = {
    init,
    connect,
    disconnect
}