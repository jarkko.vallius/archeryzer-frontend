const _ = require('lodash')
const variance = require('compute-variance');

/**
 * Add keys and values to object
 * 
 * @param {*} dataObject 
 * @param {*} keys 
 * @param {*} values 
 */
const add = (dataObject, keys, values) => {
    return _.assign({}, dataObject, _.zipObject(keys, values))
}


const timeBuffer = (sizeInMilliseconds, isFullCallback = () => { }) => {

    let buffer = []
    let isFull = false

    let obj = {}
    obj.push = dataObject => {
        buffer.push(dataObject)
        if (buffer.length > 1) {

            let index = 0
            let last = _.last(buffer)
            let diff = (last.timestamp - buffer[index].timestamp)

            if (diff >= sizeInMilliseconds) {
                while (diff >= sizeInMilliseconds) {
                    index++
                    diff = last.timestamp - buffer[index].timestamp
                }

                index--

                buffer = _.slice(buffer, index)
            }
        }
    }
    obj.size = () => buffer.length
    obj.get = () => buffer
    obj.clear = () => {
        buffer = []
        isFull = false
    }
    obj.isFull = () => isFull
    return obj
}


/**
 * Calculate a value from array
 * 
 * @param {*} param0 
 */
const calculate = ({
    array,
    type,
    bias = false,
    key,
    timeFrame
}) => {

    if (type == "avg") {
        const func = _.flow([
            arr => filter(arr, timeFrame),
            arr => _.meanBy(arr, o => o[key]),
            _.round
        ])
        return func(array)
    } else if (type == "variance") {
        const func = _.flow([
            arr => filter(arr, timeFrame),
            arr => variance(arr, { 'accessor': o => o[key], 'bias': bias }),
            _.round,
        ])
        return func(array)
    } else {
        return undefined
    }
}



/**
 * Filter DataObjects that are not in given time frame
 * 
 * @param {*} arr Array of DataObjects
 * @param {*} fromMilliseconds 
 * @param {*} toMilliseconds 
 */
const filter = (arr, timeFrame) => {
    let last = _.last(arr)
    let lastTimestamp = last.timestamp - timeFrame[0]
    let firstTimestamp = last.timestamp - timeFrame[1]
    return _.filter(arr, e => {
        return e.timestamp >= firstTimestamp && e.timestamp <= lastTimestamp
    })
}

module.exports = {
    add,
    filter,
    calculate,
    timeBuffer
}