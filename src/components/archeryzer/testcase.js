const Archeryzer = require('../src/archeryzer')
const _ = require('lodash')
const BowState = require('../src/BowState')
const ArcheryzerSet = require('../src/archeryzer-set')

const compare = async (data1, data2) => {
    return JSON.stringify(data1) == JSON.stringify(data2)
}

function createPromise() {
    var _resolve, _reject;
    var promise = new Promise(function (resolve, reject) {
        _resolve = resolve;
        _reject = reject;
    })
    promise.resolve = _resolve;
    promise.reject = _reject;
    return promise;
}

const dataObj = (timestamp, x, y, z) => {
    return {
        timestamp,
        x,
        y,
        z
    }
}

const run = async (input, config) => {
    const results = createPromise()
    const archeryzer = ArcheryzerSet({
        config: config,
        onSetEnd: payload => {
            const archeryzer2 = ArcheryzerSet({
                config: config,
                onSetEnd: payload2 => {
                    results.resolve([payload, payload2])
                }
            })
            _.map(payload, e => archeryzer2.push(e))
        }
    })
    _.map(input, e => archeryzer.push(e))
    return await results
}


const test = async () => {
    const config = {
        peakTimeFrame: [0, 1],
        peakMotionThreshold: 4,
        peakThreshold: 100,
        positionTimeFrame: [0, 10],
        positionLimit: {
            x: [0, 10],
            y: [0, 10],
            z: [0, 10]
        },
        idleToActiveTime: 3,
        activeToIdleTime: 3,
        cooldownTime: 1,
    }

    let input = [
        dataObj(1, 0, 0, 0),    // -1
        dataObj(2, 0, 0, 0),    // -1
        dataObj(3, 0, 0, 0),    // -1
        dataObj(4, 0, 0, 0),    // 0
        dataObj(5, 0, 0, 0),    // 0        -1  -1
        dataObj(6, 0, 0, 0),    // 0        -1  -1
        dataObj(7, 20, 20, 20), // 0 -> 1   -1  -1
        dataObj(8, 10, 10, 10), // 0 -> 1   -1  1
        dataObj(9, 20, 20, 20), // 0 -> 1   -1
        dataObj(10, 10, 10, 10),// 1        1
        dataObj(11, 20, 20, 20),// 1
        dataObj(12, 0, 0, 0),   // 1
        dataObj(13, 0, 0, 0),   // 1 -> 0
        dataObj(14, 0, 0, 0),   // 1 -> 0
        dataObj(15, 0, 0, 0),   // 1 -> 0
        dataObj(16, 0, 0, 0),   // 0 set end
    ]

    await run(input, config)
        .then(results => {
            // console.log("1:", results[0])
            // console.log("2:", results[1])
            //expect(results[0]).toEqual(results[1])
            console.log("end")

        })
}

test()