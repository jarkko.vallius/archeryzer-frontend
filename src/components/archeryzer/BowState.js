const BowState = {
    initializing: -1,
    idle: 0,
    active: 1,
    cooldown: 24,
    shot: 83
}

module.exports = BowState