import React, { useState, useEffect, useContext } from 'react'
import { Button, Box, CircularProgress, Divider, makeStyles, Container, Typography, Grid, ListItem, ListItemText, List, Collapse } from '@material-ui/core'
// import { PATHS } from '../../hooks/useRouter'
import { useArcheryzerData } from '../../hooks/useArcheryzerData'
import { Observer, useObserver, useLocalStore } from 'mobx-react-lite'
import { useComponentState } from '../../hooks/useComponentState'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import moment from 'moment'
import { toJS } from 'mobx'
import _ from 'lodash'
import { LineGraph } from '../LineGraph'
import { ComponentState } from '../ComponentState'
import useClippy from 'use-clippy';
import AppContext from '../../hooks/AppContext';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: "100%",
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));




const SetItem = props => {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [loading, setLoading] = useState(false)
    const { id, shotCount, index, session } = props
    const { downloadSetData } = useArcheryzerData()
    const [data, setData] = useState(null)
    const [clipboard, setClipboard] = useClippy();


    const onOpen = () => {
        if (!open) {
            setOpen(true)
            setLoading(true)
            console.log("setLoading TRUE")
            downloadSetData({ setId: id, sessionId: session.id })
                .then(results => setData(results))
                .finally(() => {
                    console.log("setLoading FALSE")
                    setLoading(false)
                })
        } else {
            setOpen(false)
        }
    }

    return (
        <>
            <ListItem button onClick={() => onOpen()} className={classes.nested}>

                <ListItemText
                    style={{ marginLeft: 40 }}
                    primary={"Set " + (index + 1)}
                    secondary={"Shots: " + shotCount}
                />

                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Divider variant="inset" component="li" />
            <Collapse in={open} timeout="auto">
                <>

                    <List component="div" disablePadding>
                        {loading ? <CircularProgress /> :
                            <>
                                {data && <LineGraph data={data} dataKey={"peak"} />}
                                <Button variant="contained" onClick={() => setClipboard(JSON.stringify(data))}>copy to clipboard</Button>

                            </>
                        }


                    </List>


                </>
            </Collapse>
        </>
    )
}

const SessionItem = props => {

    const { onDeleteSession, ...rest } = props
    const { started, ended, sets, id, shotCount } = props
    const [open, setOpen] = useState(false)

    //console.log("session props", props)



    return (
        <>
            <ListItem button onClick={() => setOpen(!open)}>
                <ListItemText
                    primary={"Session " + id}
                    secondary={moment(started).format("DD.MM.YYYY HH.mm")}
                />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Divider component="li" />
            <Collapse in={open} timeout="auto" unmountOnExit>
                <Button variant="contained" color="secondary" onClick={() => onDeleteSession(id)}>delete</Button>
                <Typography>started : {started}</Typography>
                <Typography>ended : {ended}</Typography>
                <Typography>sets : {sets.length}</Typography>
                <Typography>shotCount : {shotCount}</Typography>
                <List component="div" disablePadding>
                    {sets.map((e, index) => <SetItem key={"set_" + e.started} {...e} index={index} session={rest} />)}
                </List>
            </Collapse>
        </>
    )
}

const SessionList = props => {
    const { sessions, onDeleteSession } = props
    const classes = useStyles()

    // console.log("sessions", sessions)

    return (
        <>
            <List className={classes.root}>
                {sessions && sessions.map(s =>
                    <SessionItem
                        onDeleteSession={onDeleteSession}
                        key={"session_" + s.id}
                        {...s} />
                )}
            </List>
        </>
    )
}

const delay = t => new Promise(resolve => setTimeout(resolve, t));


const HomeView = props => {
    const { onStartSessionClick } = props
    const { setLoading, setError, componentState } = useComponentState()
    // const previousSession = sessionStorage.getItem('sessionStore')
    const store = useArcheryzerData()

    const statsStore = useLocalStore(() => ({
        shotCount: null,
        sessionCount: null,

    }))


    const fetch = () => {
        setLoading(true)
        store.fetchSessions()
            .then(results => {
                // console.log("results", results)
                const sum = _.sumBy(results, "shotCount")
                // console.log("sum", sum)
                statsStore.shotCount = sum
                statsStore.sessionCount = results.length
            })
            .catch(error => setError(error))
            .finally(() => setLoading(false))
    }

    useEffect(() => {
        fetch()
    }, [])

    const onStartSession = (clearPrevious = true) => {
        if (clearPrevious) {
            sessionStorage.removeItem("sessionStore")
        }
        onStartSessionClick()
    }

    const onDeleteSession = id => {
        setLoading(true)
        store.deleteSession(id)
            .then(fetch())
            .catch(setError)
            .finally(() => setLoading(false))
    }


    return (
        <>
            <Container>
                <Box>
                    <AppContext.Consumer>
                        {app =>
                            <>
                                <Typography variant="h6">{app.name}</Typography>
                                <Typography variant="caption">{app.version} {app.environment}</Typography>
                            </>
                        }
                    </AppContext.Consumer>
                </Box>

                {/* {previousSession &&
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => onStartSession(false)}>
                        Continue session
                    </Button>
                } */}





                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => onStartSession()}>
                    Start new session
                </Button>


                <Box py={4}>
                    <Observer>
                        {() =>
                            <>
                                <Typography>Total shots : {statsStore.shotCount}</Typography>
                                <Typography>Total sessions : {statsStore.sessionCount}</Typography>
                            </>
                        }
                    </Observer>
                </Box>

                <Typography variant="h5">Sessions</Typography>
                <Observer>
                    {() =>
                        <>
                            <ComponentState {...componentState}>
                                <SessionList sessions={store.sessions} onDeleteSession={onDeleteSession} />
                            </ComponentState>
                        </>
                    }
                </Observer>
            </Container>
        </>
    )
}

export { HomeView }

