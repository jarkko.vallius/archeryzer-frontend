import React, { useEffect, useState } from 'react'
// import { Line } from 'react-chartjs-2';
import moment from 'moment'
import _ from 'lodash'
import { createMuiTheme } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors'
import { useLocalStore, Observer } from 'mobx-react-lite'
import { Label, ReferenceLine, Tooltip, LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer } from 'recharts';

// const theme = createMuiTheme()
// console.log("muitheme", theme)

// const lineColor = theme.palette.primary.light

const CustomLabel = props => {
    console.log("CustomLabel", props)
    return (
        <>
            hello
        </>
    )
}

const LineGraph = props => {
    const { data, dataKey, shots } = props

    const convertTimestamp = t => t // moment(t).format("HH:mm.ss,SSS")

    const [graphData, setGraphData] = useState([])

    useEffect(() => {
        setGraphData(data.map(e => {
            const obj = JSON.parse(JSON.stringify(e))
            obj.timestamp = convertTimestamp(obj.timestamp)
            return obj
        }))

    }, [data])


    return (
        <>
            <ResponsiveContainer width="100%" height={400}>
                <LineChart data={graphData} margin={{ top: 5, right: 5, bottom: 50, left: 5 }}>
                    <Line
                        isAnimationActive={false}
                        dot={false}
                        type="monotone"
                        dataKey={dataKey}
                        stroke={blue[400]}
                        strokeWidth={2}
                    />
                    <CartesianGrid
                        stroke="#eee" />
                    <XAxis
                        orientation="top"
                        //type="number"
                        dataKey="timestamp" hide={true}>

                    </XAxis>
                    <YAxis />
                    <Tooltip />
                    {shots.map((e, index) =>
                        <ReferenceLine
                            x={_.findIndex(data, d => d.timestamp === e.timestamp) + 1}
                            stroke="red"
                        >
                            <Label position="bottom" value={index + 1} />
                        </ReferenceLine>)}
                </LineChart>
            </ResponsiveContainer >
        </>

    )
}

LineGraph.defaultProps = {
    shots: []
}

export { LineGraph }
