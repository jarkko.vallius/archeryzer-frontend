import { Box, CircularProgress, makeStyles } from '@material-ui/core';
import React from 'react';


const useStyles = makeStyles((theme) => ({
    spinner: {
        position: "absolute",
        top: "50%",
        left: "50%"
    },
    error: {
        backgroundColor: "red",
        color: "white"
    }
}));

const ComponentState = props => {
    const { children, loading, error } = props
    const classes = useStyles()

    return (
        <>
            {error && <Box className={classes.error}>{error.toString()}</Box>}
            {children}
            {loading && <CircularProgress className={classes.spinner} />}
        </>
    )
}

export { ComponentState }