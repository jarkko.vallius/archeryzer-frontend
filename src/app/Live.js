import React from 'react'
import { useHistory } from 'react-router-dom'
import { LiveView } from '../components/live/LiveView'

const Live = props => {
    const history = useHistory()
    return <LiveView
        onBackClick={() => history.goBack()}
    />
}

export { Live }
