import React from 'react'
import { useHistory } from 'react-router-dom'
import { HomeView } from '../components/home/HomeView'
import { PATH } from '../hooks/useRouter'


const Home = props => {
    const history = useHistory()

    const onStartSessionClick = () => {
        console.log("onStartSessionClick")
        console.log("history", history)
        history.push(PATH.live)
    }

    return <HomeView
        onStartSessionClick={onStartSessionClick}
    />

}

export { Home }

