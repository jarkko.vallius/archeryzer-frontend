import React from 'react';
import { Home } from '../app/Home'
import { Live } from '../app/Live'

const PATH = {
    home: "/",
    live: "/live"
}

const ROUTES = [
    {
        path: PATH.home,
        exact: true,
        render: props => <Home {...props} />
    },
    {
        path: PATH.live,
        render: props => <Live {...props} />
    }
]

export { ROUTES, PATH }