import React from "react";
import pkg from '../../package.json'

const AppContext = React.createContext({
    name: "Archeryzer",
    version: pkg.version,
    environment: process.env.NODE_ENV
});

export default AppContext;