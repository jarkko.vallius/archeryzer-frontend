import axios from 'axios'
import React, { useState } from 'react';
import { useFirebase } from './useFirebase'

// https://firebase.google.com/docs/storage/web/upload-files

/**
 * to upload/download json files to/from google firebase
 * 
 * @param {*} props 
 */
const useStorage = props => {
    const { firebase } = useFirebase()

    const upload = ({ path, data }) => {
        return new Promise((resolve, reject) => {
            var storageRef = firebase.storage().ref(path);
            var uploadTask = storageRef.putString(JSON.stringify(data))
            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                function (snapshot) {
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            console.log('Upload is running');
                            break;
                        default:
                    }
                }, function (error) {
                    // A full list of error codes is available at
                    // https://firebase.google.com/docs/storage/web/handle-errors
                    reject(error)
                    switch (error.code) {
                        case 'storage/unauthorized':
                            // User doesn't have permission to access the object
                            break;
                        case 'storage/canceled':
                            // User canceled the upload
                            break;
                        case 'storage/unknown':
                            // Unknown error occurred, inspect error.serverResponse
                            break;
                        default:
                    }
                }, function () {
                    // Upload completed successfully, now we can get the download URL
                    uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                        console.log('File available at', downloadURL);
                        resolve(downloadURL)
                    });
                });
        })

    }

    const download = ({ path }) => {
        return new Promise((resolve, reject) => {
            var storageRef = firebase.storage().ref(path);
            storageRef.getDownloadURL()
                .then(function (url) {
                    console.log("URL", url)
                    axios.get(url)
                        .then(results => {
                            resolve(results.data)
                        })
                })
                .catch(error => reject(error))
        })
    }

    return { upload, download }

}

export { useStorage }