import { useState } from 'react'

const useComponentState = () => {
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const handleError = payload => {
        console.error(payload)
        setError(payload)
    }
    return { componentState: { loading, error }, setLoading, setError: handleError }
}

export { useComponentState }