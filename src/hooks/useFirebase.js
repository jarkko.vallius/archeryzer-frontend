import * as firebase from "firebase/app";
// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";
// Add the Firebase products that you want to use
import "firebase/storage";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIRESTORE_APIKEY || "",
    authDomain: process.env.REACT_APP_FIRESTORE_AUTHDOMAIN || "",
    databaseURL: process.env.REACT_APP_FIRESTORE_DATABASEURL || "",
    projectId: process.env.REACT_APP_FIRESTORE_PROJECTID || "notset",
    storageBucket: process.env.REACT_APP_FIRESTORE_STORAGEBUCKET || "",
    messagingSenderId: process.env.REACT_APP_FIRESTORE_MESSAGINGSENDERID || "",
    appId: process.env.REACT_APP_FIRESTORE_APPID || "",
    measurementId: process.env.REACT_APP_FIRESTORE_MEASUREMENTID || ""
};

const useFirebase = () => {
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    const db = firebase.firestore()
    // const storage = firebase.storage()
    return { db, firebase }
}

export { useFirebase }

