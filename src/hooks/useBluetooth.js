import React, { useEffect } from 'react'

var NUS_SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
var NUS_TX_CHARACTERISTIC_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
var NUS_RX_CHARACTERISTIC_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";

var bluetoothDevice = null;
var txCharacteristic = null;
var rxCharacteristic = null;

const useBluetooth = props => {
    const { onData, onStatusChange } = props

    useEffect(() => {
        onStatusChange({ name: "ready" })
    }, [])


    const connect = () => {
        onStatusChange({ name: "connecting" })
        let serviceUuid = NUS_SERVICE_UUID;
        let tx_characteristicUuid = NUS_TX_CHARACTERISTIC_UUID;
        let rx_characteristicUuid = NUS_RX_CHARACTERISTIC_UUID;

        console.log('Requesting Bluetooth Device...');
        navigator.bluetooth.requestDevice({ filters: [{ services: [serviceUuid] }] })
            .then(device => {
                bluetoothDevice = device;
                bluetoothDevice.addEventListener('gattserverdisconnected', onDisconnected);

                console.log('Connecting to GATT Server...');
                return device.gatt.connect();
            })
            .then(server => {
                console.log('Getting Service...');
                return server.getPrimaryService(serviceUuid);
            })
            .then(service => {
                console.log('Getting Characteristics...');
                return Promise.all([
                    service.getCharacteristic(tx_characteristicUuid)
                        .then(characteristic => {
                            txCharacteristic = characteristic;
                            console.log('Tx charactersitic obtained');
                            return txCharacteristic.startNotifications().then(_ => {
                                console.log('Notifications started');
                                txCharacteristic.addEventListener('characteristicvaluechanged', handleNotifications);
                                onStatusChange({ name: "connected", device: bluetoothDevice })
                            });
                        }),
                    service.getCharacteristic(rx_characteristicUuid)
                        .then(rx_characteristic => {
                            rxCharacteristic = rx_characteristic;
                            console.log('Rx charactersitic obtained');
                        }),
                ]);
            })
            .catch(error => {
                onStatusChange({ status: "error", error: error })
                console.log('Argh! ' + error);
            });
    }


    const disconnect = () => {
        console.log("disconnecting")

        if (bluetoothDevice && bluetoothDevice.gatt.connected) {
            console.log('Disconnecting from Bluetooth Device...');
            bluetoothDevice.gatt.disconnect();
        }

    }

    function onDisconnected(event) {
        console.log('Bluetooth Device disconnected');

        if (txCharacteristic) {
            console.log('Stopping notifications...');
            try {
                txCharacteristic.removeEventListener('characteristicvaluechanged', handleNotifications);
            }
            catch (error) {
                console.error('useBluetooth error ');
                console.error(error)
            }
            txCharacteristic = null;
        }

        txCharacteristic = null;
        rxCharacteristic = null;

        onStatusChange({ name: "disconnected" })
    }

    const handleNotifications = event => {
        let value = event.target.value;
        onData(value)
    }

    return { connect, disconnect }

}

// BluetoothController.defaultProps = {
//     children: () => false,
//     onData: () => false
// }

export { useBluetooth }
export default useBluetooth