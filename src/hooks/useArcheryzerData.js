import { useLocalStore } from 'mobx-react-lite';
import { useFirebase } from './useFirebase';
import { useStorage } from './useStorage';

// const get = key => {
//     let d = localStorage.getItem(key)
//     if (d != null) {
//         return JSON.parse(d)
//     } else return d
// }

// const set = (key, value) => {
//     return localStorage.setItem(key, JSON.stringify(value))
// }



const useArcheryzerData = () => {
    const { db, firebase } = useFirebase()
    const { upload, download } = useStorage()

    const store = useLocalStore(() => ({
        sessions: null,
        fetchSessions: () => {
            let results = []
            console.log("fetching")
            console.log("db", db)
            return db.collection("sessions")
                .get()
                .then(function (querySnapshot) {
                    querySnapshot.forEach(doc => {
                        results.push(doc.data())
                    })
                })
                .then(() => {
                    store.sessions = results
                    return results
                })

        },
        setSession: ({ session }) => {
            const cpy = JSON.parse(JSON.stringify(session))
            if (!cpy.created) cpy.created = firebase.firestore.FieldValue.serverTimestamp()
            cpy.updated = firebase.firestore.FieldValue.serverTimestamp()
            return db.collection("sessions").doc(cpy.id).set(cpy)
        },
        deleteSession: (id) => {
            return db.collection("sessions").doc(id).delete()
        },
        setSet: ({ session, set }) => {
            const cpy = JSON.parse(JSON.stringify(set))
            if (!cpy.created) cpy.created = firebase.firestore.FieldValue.serverTimestamp()
            cpy.updated = firebase.firestore.FieldValue.serverTimestamp()
            return db.collection("sessions").doc(session.id)
                .collection("sets").doc(cpy.id)
                .set(cpy)
        },
        uploadSetData: ({ setId, sessionId, data }) => {
            const path = sessionId + "/" + setId + ".json"
            return upload({ path: path, data: data })
        },
        downloadSetData: ({ setId, sessionId }) => {
            const path = sessionId + "/" + setId + ".json"
            return download({ path: path })
        }
    }))
    return store
}

export { useArcheryzerData };

