import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { ROUTES } from './hooks/useRouter'

const App = () => {
    return (
        <>
            <BrowserRouter>
                <Switch>
                    {ROUTES.map(r => <Route key={"route_" + r.path} {...r} />)}
                </Switch>
            </BrowserRouter>
        </>
    )
}

export default App;
